<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$router->get('/user', 'UsersController@index');
$router->get('/user/{id}', 'UsersController@show');
$router->post('/user', 'UsersController@store');
$router->put('/user/{id}', 'UsersController@update');


// Route::get('/', function () {
//     return view('welcome');
// });
