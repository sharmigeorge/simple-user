<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Users;
use App\Http\Helpers\Users as UsersHelper;

class UsersController extends Controller
{

	/**
	 * The request instance.
	 *
	 * @var \Illuminate\Http\Request
	 */
	private $request;

	/**
	* @var string
	*/
	protected $resource = 'users';

	/**
	 * The Class that handles the request and stuff
	 *
	 * @var
	 */
	protected $helper;

   	public function __construct(UsersHelper $helper, 
								Request $request
								)
	{
		$this->helper = $helper;
		$this->request = $request;
	}

	public function index(): JsonResponse
	{
		$response = $this->helper->getAll();
		 if(!isset($response['error']))
		{
			return response()->json([
				'code' => '200',
				'message' => 'Request Successfully Excecuted!',
				'data' => $response,
			], 200); 
		}
			
		return response()->json([
			'code' => '400',
			'error' => $response['error'],
			'message' => 'Unable To Submit The Request!',
		], 400);
	}

	public function show(int $id): JsonResponse
	{
		$response = $this->helper->getByID($id, []);
		if(!isset($response['error']))
		{
			return response()->json([
				'code' => '200',
				'message' => 'Request Successfully Excecuted!',
				'data' => [$response],
			], 200); 
		}
			
		return response()->json([
			'code' => '400',
			'error' => $response['error'],
			'message' => 'Request not successfull!',
		], 400);
	}

	public function store(): JsonResponse
	{
		$data = $this->request->all();
		$this->validateRequest($data);

		$response = $this->helper->create($data);

		if(!isset($response['error']))
		{
			return response()->json([
				'code' => '201',
				'message' => 'Created Successfully!',
				'data' => [$response],
			], 201); 
		}
			
		return response()->json([
			'code' => '400',
			'error' => $response['error'],
			'message' => 'Unable To Submit The Request!',
		], 400);
	}

	public function update(int $id): JsonResponse
	{
		$data = $this->request->all();

		$response = $this->helper->update($id, $data);

		if(!isset($response['error']))
			return response()->json([
				'code' => '200',
				'message' => 'Updated Successfully!',
			], 200);

		return response()->json([
			'code' => '400',
			'error' => $response['error'],
			'message' => 'Unable to Update the Data!',
		], 400); 
	}

	private function validateRequest($request) 
	{
		$return = [];
		$rules = [
			'name' => 'required|min:3',
			'email' => 'required|email|unique:users',
			'password' => 'required|min:8',
		];
		$validation_messages = [
			'name.required' => 'Name field has to be filled',
			'name.min' => 'Please provide a legit name',
			'email.required' => 'email field has to be filled',
			'email.unique' => 'An account already exist with this email',
            'email.email' => 'email validation failed',
            'password.required' => 'password field has to be filled',
		];

		$validator = Validator::make($request,$rules,$validation_messages);

		if ($validator->fails()) 
        {
            $return['status'] =  400;
            $return['message'] = $validator->errors();
            return $return;
        }

        $return['status'] = 200;
        $return['message'] = 'Validated successfully';
        return $return;
	}
}
