<?php

/*
* Copyright (C) Kaleyra  - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Kaleyra Developers <products@kaleyra.com>, September 2018
*/

namespace App\Http\Helpers;

use Illuminate\Support\Facades\Hash;
use App\User as UsersModel;

/**
 * JWT Auth Helper class
 *
 * @since  0.0.1
 *
 * @author sharmi george <sharmi.george@kaleyra.com> 
 */
class Users{

	/**
	 * @var Model
	 */
	protected $model;

	/**
	 * @var null
	 */
	private $toSelect = null;

	public function __construct(UsersModel $model)
	{
		$this->model = $model;
	}

	public function create(array $data): array
	{
		$user = $this->model->where('email', $data['email'])
							->first();
		if($user)
			return ['error' => "This email is already in use" ];

		$user = $this->model->where('email', $data['email'])
							->first();

		if(!$user) {

			$user = $this->model->newInstance();

			$user->password = Hash::make($data['password']);
			$user->email = $data['email'];
			$user->name = $data['name'];

			isset($data['status']) ? $user->status = $data['status'] : $user->status = 1;

			$user->save();
		} 
		return $user->toArray();
	}

	public function update(string $id, array $data = []): array
	{
		$toUpdate = array_only($data, [
			'name',
		]);
		$this->model->where('id', '=', $id)
					->update($toUpdate);
		return ['done' => true];
	}

	public function getAll($with = []): array
	{
		$query = $this->make($with);
		return $query->get()->toArray();
	}

	public function getByID(int $id, $with = []): array
	{
		$query = $this->make($with);
		$data = $query->find($id);
		if ($data)
			return $data->toArray();

        return ['error' => "Resource not found" ];
		
	}

	public function make(array $relations = [])
	{
		$query = $this->model;
		$toSelect = $this->toSelect;

		if (!is_array($toSelect)) {
			$query = $query->with($relations);
		} else {
			foreach ($relations as $relation) {
				// Check if relation has select field defined
				if (isset($toSelect[$relation])) {
					$select = $toSelect[$relation];
					$query = $query->with([$relation => function ($q) use ($select) {
						$q->select($select);
					}]);
					unset($toSelect[$relation]);
				} else {
					$query = $query->with($relation);
				}
			}
			$query = $query->select($toSelect);
		}
		return $query;
	}
}
